# haXlab Toolkit for Nest Cam IQ Indoor

Please visit https://haxlab.atlassian.net/wiki/spaces/NCI/overview for the most up to date documentation.

The docs folder contains the most up to date documentation for this fixture.

The scripts folder contains scripts that are found in ~/haXlab-nest-cam.